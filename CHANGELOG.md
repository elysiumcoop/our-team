# Changelog

# 1.x Branch - Our Team by WooThemes
This is the branch when the plugin was referred to as "Our Team by WooThemes".

## 1.4.1
* 2015-07-07
* Removes deprecated constructor call for WP_Widget

## 1.4.0
* New - Team member contact email field.
* New - Team member telephone number field.
* New - Team member details now output on single / archive templates.
* Tweak - WordPress user mapping label clearer.
* Tweak - Added a documentation link to the plugin action links.
* Tweak - Added a help tab to new/edit post screens.
* Fix - Undefined index notice. Kudos @apenchev.

## 1.3.1
* Tweak - More tag works as expected.
* Tweak - Updated default slug for Team Taxonomy to 'team-members'.

## 1.3.0
* New - Filters for taxonomy args, post type args and individual team member template. Kudos @helgatheviking.
* New - You can now use the 'slug' parameter in the shortcode to display an individual team member by slug.
* Fix - WordPress users can now be unassigned from a team member.

## 1.2.0
* New - Team members can be sorted by meta. Kudos @helgatheviking.
* Tweak - Renamed woothemes_our_member_fields filter to woothemes_our_team_member_fields. Kudos @tlehtimaki.
* Tweak - Default args now filterable via woothemes_our_team_default_args.
* Localization - Changed textdomain to match plugin slug. Kudos @cfoellmann.

## 1.1.0
* New - Team members can be assigned to a WordPress user pulling a link to their posts into the team page and replacing their description with the bio according to their profile.
* 3.8 UI compatibility.

## 1.0.2
* Fixed typo in the team members archive slug. Kudos digitales.
* title, before_title and after_title params are now passed to shortcode.
* Team member css class is now filterable allowing custom unique clases.
* Prepended all filters woothemes_our_team_

## 1.0.1
* Added role & twitter args to shortcode.
* Added role option to widget.
* Display 12 team members by default (previously 5).
* Typos and other minor fixes.

## 1.0.0
* Initial release. Woo!